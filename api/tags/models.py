from django.db import models
from drivers.models import Driver
from vehicles.models import Vehicle


# Create your models here.
class Tag(models.Model):
    name = models.CharField(max_length=128, unique=True)
    vehicles = models.ManyToManyField(Vehicle, blank=True)
    drivers = models.ManyToManyField(Driver, blank=True)

    def __str__(self):
        return self.name
