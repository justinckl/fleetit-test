from rest_framework import serializers

from .models import Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Tag


class CreateTagSerializer(serializers.ModelSerializer):
    tag = serializers.CharField(max_length=50)

    class Meta:
        fields = ["tag"]
        model = Tag
