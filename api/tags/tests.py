from django.test import Client, TestCase


# Create your tests here.
class TagTestCase(TestCase):
    def setUp(self):
        self.url = "/tags/"
        self.client = Client()

    def test_create_tag(self):
        data = {"name": "tag1"}
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "tag1")

    def test_create_tag_missing_field(self):
        data = {}
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["name"][0], "This field is required.")
