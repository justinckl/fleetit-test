from django.shortcuts import render
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from tags.models import Tag
from tags.serializers import CreateTagSerializer, TagSerializer

from .models import Driver, VehicleDriver
from .serializers import DriverSerializer, VehicleDriverSerializer


# Create your views here.
class DriverViewSet(viewsets.ModelViewSet):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer
    filterset_fields = ["tag__name"]
    search_fields = ["tag__name"]

    def get_serializer_class(self):
        if self.action == "tags":
            return CreateTagSerializer
        return DriverSerializer

    @action(
        detail=True,
        methods=[
            "post",
            "get",
        ],
    )
    def tags(self, request, pk=None):
        """
        Manage Tags for a driver

        POST /api/drivers/1/tags/

        If the tag exists, it will be added to the driver.
        If the tag does not exist, it will be created and added to the driver.

        GET /api/drivers/1/tags/

        Returns a list of all tags assigned to the driver

        """
        if request.method == "GET":
            driver = self.get_object()
            tags = driver.tag_set.all()
            serializer = TagSerializer(tags, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        elif request.method == "POST":
            driver = self.get_object()
            serializer = CreateTagSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            try:
                tag = Tag.objects.get(name=serializer.data["tag"])
                driver.tag_set.add(tag)
                return Response(
                    self.get_serializer(driver).data, status=status.HTTP_201_CREATED
                )
            except Tag.DoesNotExist:
                tag = Tag.objects.create(name=serializer.data["tag"])
                driver.tag_set.add(tag)
                return Response(
                    DriverSerializer(driver).data, status=status.HTTP_201_CREATED
                )

    @action(detail=True, methods=["delete"], url_path="tags/(?P<tag_name>.+)")
    def delete_tag(self, request, pk=None, tag_name=None):
        """
        Delete a tag from a driver

        DELETE /api/drivers/1/tags/{tag_name}/

        If the tag exists, it will be removed from the driver. This api does not explicitly check whether the tag is assigned to the driver.
        If the tag does not exist, it will return a 200 response.
        """
        driver = self.get_object()
        try:
            tag = Tag.objects.get(name=tag_name)
            driver.tag_set.remove(tag)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Tag.DoesNotExist:
            return Response(status=status.HTTP_204_NO_CONTENT)
