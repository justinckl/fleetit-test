from rest_framework import serializers

from .models import Driver, VehicleDriver


class DriverSerializer(serializers.ModelSerializer):
    tags = serializers.StringRelatedField(many=True, source="tag_set", read_only=True)

    class Meta:
        fields = "__all__"
        model = Driver


class VehicleDriverSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = VehicleDriver
