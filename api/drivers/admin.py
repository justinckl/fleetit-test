from django.contrib import admin

from .models import Driver, VehicleDriver

# Register your models here.


@admin.register(Driver)
class DriverAdmin(admin.ModelAdmin):
    pass


@admin.register(VehicleDriver)
class VehicleDriverAdmin(admin.ModelAdmin):
    pass
