from django.test import Client, TestCase


# Create your tests here.
class DriverTestCase(TestCase):
    def setUp(self):
        self.url = "/drivers/"
        self.client = Client()

    def test_create_driver(self):
        data = {
            "first_name": "John",
            "last_name": "Doe",
            "dob": "1980-01-01",
            "license_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["first_name"], "John")
        self.assertEqual(response.data["last_name"], "Doe")
        self.assertEqual(response.data["dob"], "1980-01-01")
        self.assertEqual(response.data["license_number"], "123456789")

    def test_create_driver_missing_field(self):
        data = {
            "first_name": "John",
            "last_name": "Doe",
            "dob": "1980-01-01",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["license_number"][0], "This field is required.")

    def test_create_driver_invalid_field(self):
        data = {
            "first_name": "John",
            "last_name": "Doe",
            "dob": "sdfsdfsdf",
            "license_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.data["dob"][0],
            "Date has wrong format. Use one of these formats instead: YYYY-MM-DD.",
        )

    def test_create_driver_add_tag(self):
        data = {
            "first_name": "John",
            "last_name": "Doe",
            "dob": "1980-01-01",
            "license_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["first_name"], "John")
        self.assertEqual(response.data["last_name"], "Doe")
        self.assertEqual(response.data["dob"], "1980-01-01")
        self.assertEqual(response.data["license_number"], "123456789")

        data = {"tag": "tag1"}
        response = self.client.post(
            self.url + str(response.data["id"]) + "/tags/", data=data
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["first_name"], "John")
        self.assertEqual(response.data["last_name"], "Doe")
        self.assertEqual(response.data["dob"], "1980-01-01")
        self.assertEqual(response.data["license_number"], "123456789")
        self.assertEqual(response.data["tags"][0], "tag1")
