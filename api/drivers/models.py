from django.db import models
from django.utils import timezone
from vehicles.models import Vehicle


# Create your models here.
class Driver(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    dob = models.DateField()
    license_number = models.CharField(max_length=32)


class VehicleDriver(models.Model):
    vehicle = models.ForeignKey(
        Vehicle, on_delete=models.CASCADE, related_name="vehicle_driver"
    )
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(null=True, blank=True)
