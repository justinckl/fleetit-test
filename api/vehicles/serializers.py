from rest_framework import serializers
from tags.models import Tag
from tags.serializers import TagSerializer

from .models import Vehicle


class VehicleSerializer(serializers.ModelSerializer):
    tags = serializers.StringRelatedField(many=True, source="tag_set", read_only=True)

    class Meta:
        fields = "__all__"
        model = Vehicle
