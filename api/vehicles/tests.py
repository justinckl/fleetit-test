from django.test import Client, TestCase

# Create your tests here.


class VehicleTestCase(TestCase):
    def setUp(self):
        self.url = "/vehicles/"
        self.client = Client()

    def test_create_vehicle(self):
        data = {
            "make": "Honda",
            "model": "Civic",
            "year": "2019",
            "plate_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["make"], "Honda")
        self.assertEqual(response.data["model"], "Civic")
        self.assertEqual(response.data["year"], 2019)
        self.assertEqual(response.data["plate_number"], "123456789")

    def test_create_vehicle_missing_field(self):
        data = {
            "make": "Honda",
            "model": "Civic",
            "year": "2019",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["plate_number"][0], "This field is required.")

    def test_create_vehicle_invalid_field(self):
        data = {
            "make": "Honda",
            "model": "Civic",
            "year": "sdfsdfsdf",
            "plate_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["year"][0], "A valid integer is required.")

    def test_create_vehicle_add_tag(self):
        data = {
            "make": "Honda",
            "model": "Civic",
            "year": "2019",
            "plate_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["make"], "Honda")
        self.assertEqual(response.data["model"], "Civic")
        self.assertEqual(response.data["year"], 2019)
        self.assertEqual(response.data["plate_number"], "123456789")

        data = {"tag": "tag1"}
        response = self.client.post(
            self.url + str(response.data["id"]) + "/tags/", data=data
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["make"], "Honda")
        self.assertEqual(response.data["model"], "Civic")
        self.assertEqual(response.data["year"], 2019)
        self.assertEqual(response.data["plate_number"], "123456789")
        self.assertEqual(response.data["tags"][0], "tag1")

    def test_create_vehicle_add_tag_missing_field(self):
        data = {
            "make": "Honda",
            "model": "Civic",
            "year": "2019",
            "plate_number": "123456789",
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["make"], "Honda")
        self.assertEqual(response.data["model"], "Civic")
        self.assertEqual(response.data["year"], 2019)
        self.assertEqual(response.data["plate_number"], "123456789")

        data = {}
        response = self.client.post(
            self.url + str(response.data["id"]) + "/tags/", data=data
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["tag"][0], "This field is required.")
