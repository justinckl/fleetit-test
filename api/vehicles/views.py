from django.utils import timezone
from django_filters import rest_framework as filters
from drivers.models import Driver, VehicleDriver
from drivers.serializers import DriverSerializer, VehicleDriverSerializer
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from tags.models import Tag
from tags.serializers import CreateTagSerializer, TagSerializer

from .models import Vehicle
from .serializers import VehicleSerializer


# Create your views here.
class VehicleViewSet(viewsets.ModelViewSet):
    """
    list:
    Returns a list of all vehicles. You can filter by tag name using the tag query parameter. You can also search by tag name using the search query parameter.
    """

    queryset = Vehicle.objects.all()
    filterset_fields = ["tag__name"]
    search_fields = ["tag__name"]
    serializer_class = VehicleSerializer

    def get_serializer_class(self):
        if self.action == "drivers":
            return VehicleDriverSerializer
        if self.action == "tags":
            return CreateTagSerializer
        return VehicleSerializer

    @action(
        detail=True,
        methods=[
            "post",
            "get",
        ],
    )
    def tags(self, request, pk=None):
        """
        Manage Tags for a vehicle

        POST /api/vehicles/1/tags/

        If the tag exists, it will be added to the vehicle.
        If the tag does not exist, it will be created and added to the vehicle.

        GET /api/vehicles/1/tags/

        Returns a list of all tags assigned to the vehicle

        """
        if request.method == "GET":
            vehicle = self.get_object()
            tags = vehicle.tag_set.all()
            serializer = TagSerializer(tags, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        elif request.method == "POST":
            vehicle = self.get_object()
            serializer = CreateTagSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            try:
                tag = Tag.objects.get(name=serializer.data["tag"])
                vehicle.tag_set.add(tag)
                return Response(
                    self.get_serializer(vehicle).data, status=status.HTTP_201_CREATED
                )
            except Tag.DoesNotExist:
                tag = Tag.objects.create(name=serializer.data["tag"])
                vehicle.tag_set.add(tag)
                return Response(
                    VehicleSerializer(vehicle).data, status=status.HTTP_201_CREATED
                )

    @action(detail=True, methods=["delete"], url_path="tags/(?P<tag_name>.+)")
    def delete_tag(self, request, pk=None, tag_name=None):
        """
        Delete a tag from a vehicle

        DELETE /api/vehicles/1/tags/{tag_name}/

        If the tag exists, it will be removed from the vehicle. This api does not explicitly check whether the tag is assigned to the vehicle.
        If the tag does not exist, it will return a 200 response.
        """
        vehicle = self.get_object()
        try:
            tag = Tag.objects.get(name=tag_name)
            vehicle.tag_set.remove(tag)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Tag.DoesNotExist:
            return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=["get", "post"])
    def drivers(self, request, pk=None):
        """
        POST /api/vehicles/{id}/drivers/

        Adds a driver to a vehicle
        If the vehicle is already assigned to a driver, it will return a 400 error. In order to assign a new driver, the current driver must be unassigned first using the unassign_driver action.

        GET /api/vehicles/{id}/drivers/

        Returns a list of all drivers assigned to the vehicle
        """
        if request.method == "GET":
            vehicle = self.get_object()
            drivers = vehicle.vehicle_driver.all()
            serializer = VehicleDriverSerializer(drivers, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        elif request.method == "POST":
            vehicle = self.get_object()
            serializer = VehicleDriverSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            try:
                driver = Driver.objects.get(id=serializer.data["driver"])
            except Driver.DoesNotExist:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                # Check if the vehicle is already assigned to some driver
                # The end_date is null if the vehicle is currently assigned to a driver
                vehicle_driver = VehicleDriver.objects.filter(
                    vehicle=vehicle, end_date__isnull=True
                )
                if vehicle_driver:
                    # check if the vehicle is already assigned to the same driver
                    if vehicle_driver[0].driver == driver:
                        return Response(
                            data={
                                "message": "Vehicle is already assigned to this driver"
                            },
                            status=status.HTTP_400_BAD_REQUEST,
                        )
                    return Response(
                        data={
                            "message": "Vehicle is already assigned to another driver"
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                vehicle_driver = VehicleDriver.objects.create(
                    driver=driver, vehicle=vehicle
                )
                return Response(
                    VehicleDriverSerializer(vehicle_driver).data,
                    status=status.HTTP_201_CREATED,
                )

    @action(detail=True, methods=["post"])
    def unassign_driver(self, request, pk=None):
        """
        Unassigns the current driver from the vehicle.

        If the vehicle is not assigned to any driver, it will return a 400 error
        If the vehicle is assigned to a driver, it will unassign the driver from the vehicle


        Returns:
        - Response: A DRF Response object.
          - Example: {"message": "Vehicle is not assigned to any driver"}
        """
        vehicle = self.get_object()
        vehicle_driver = VehicleDriver.objects.filter(
            vehicle=vehicle, end_date__isnull=True
        )
        if vehicle_driver:
            vehicle_driver[0].end_date = timezone.now()
            vehicle_driver[0].save()
            return Response(
                data={"message": "Vehicle is unassigned from the driver"},
                status=status.HTTP_200_OK,
            )
        else:
            return Response(
                data={"message": "Vehicle is not assigned to any driver"},
                status=status.HTTP_400_BAD_REQUEST,
            )
