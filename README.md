![FleetIT Logo](https://fleetit.com/_next/static/media/fleetit-logo.fbad3e68.svg)

# Submission For Software Engineer Hiring Challenge

Welcome to this repository, which serves as the solution submitted for the Software Engineer Hiring Process challenge. In this project, I have enhanced an existing Django-based CRUD application designed for managing vehicles and drivers.

## Challenge Overview

### Requirements

1. **Adding Tagging Feature:**
   - [x] Modify the existing Django application provided in the `api` folder to include the ability to add tags to vehicles and drivers.
   - [x] Create a new database model for tags and establish a relationship with the vehicle and driver models.

2. **Vehicle-Driver Relationship:**
   - [x] Enhance the application to establish a relationship between vehicles and drivers.
   - [x] A driver can drive multiple vehicles.
   - [x] A vehicle can have multiple drivers.
   - [x] Record when a vehicle was driven by a driver.

3. **API Endpoints:**
   - Create API endpoints to perform the following actions:
     - [x] Create a new tag.http://127.0.0.1:8000/redoc/#tag/tags/operation/createTag
     - [x] List all tags. http://127.0.0.1:8000/redoc/#tag/tags/operation/listTags
     - [x] Add one or more tags to a vehicle. http://127.0.0.1:8000/redoc/#tag/vehicles/operation/tagsVehicle
     - [x] Add one or more tags to a driver. http://127.0.0.1:8000/redoc/#tag/drivers/operation/tagsDriver
     - [x] List all tags associated with a vehicle or driver.
     - [x] Search for vehicles by tag.
     - [x] Search for drivers by tag.

     Searching can be performed by using the `?search` query parameter.

Please Note that all the documentation for the API endpoints can be found in the http://localhost:8000/redoc/ page. 

4. **Bonus Points (Optional):**
   - [x]  Document your API thoroughly, explaining how to use the endpoints and what responses to expect. 
      > Documentation for API is added docstrings in the code. It is also available in the http://localhost:8000/redoc/ page.
   - [x] Write unit tests to validate the correctness of your code and include a test suite.
      > Unit tests are added in the tests.py file in the app folder. The tests can be executed by running the following command.
      ```bash
      python manage.py test
      ```
   - [x] Consider providing Docker support with a Dockerfile and instructions for running the application in a Docker container.
      > Dockerfile is added in the root directory of the project. Instructions for running the application in a Docker container is added below.


### Running Docker Container
***Note:*** *Make sure you have docker installed in your system.*

1. Clone the repository.
2. Open the terminal and navigate to the root directory of the project.
3. Run the following command to build the docker image.
   ```bash
   docker build -t fleetit .
   ```
4. Run the following command to run the docker container.
   ```bash
   docker run -p 8000:8000 fleetit
   ```
5. Creating Superuser.
   **Enter the docker container by running the following command.**
   ```bash
   docker exec -it <container_id> bash
   ```   
   **Run the following command to create a superuser.**
   ```bash
   python manage.py createsuperuser
   ```

5. Open the browser and navigate to http://localhost:8000/redoc/ to view the documentation of the API endpoints.
6. Open the browser and navigate to http://localhost:8000/admin/ to view the admin panel.
8. You can also run the following command to run the docker container in the background.
   ```bash
   docker run -d -p 8000:8000 fleetit
   ```

### Running Without Docker Container
***Note:*** *Make sure you have python3 and pip installed in your system.*

1. Clone the repository.
2. Open the terminal and navigate to the root directory of the project.
3. Run the following command to install the dependencies.
   ```bash
   pip install -r requirements.txt
   ```
4. Run the following command to run the migrations.
   ```bash
   pythjon manage.py makemigrations
   python manage.py migrate
   ```
4. Run the following command to run the server.
   ```bash
   python manage.py runserver
   ```
6. Create a superuser by running the following command.
   ```bash
   python manage.py createsuperuser
   ```
5. Open the browser and navigate to http://localhost:8000/redoc/ to view the documentation of the API endpoints.
6. Open the browser and navigate to http://localhost:8000/admin/ to view the admin panel.